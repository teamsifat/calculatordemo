package com.sifatullah.firstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {

    Button one,two,three,four,five,six,seven,eight,nine,zero,plus,minus,into,div,percent,equal,clear;
    TextView screenTv;
    String exp;
    double number1,number2;
    double result;
    int operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_new);



        one=findViewById(R.id.one);
        two=findViewById(R.id.two);
        three=findViewById(R.id.three);
        four=findViewById(R.id.four);
        five=findViewById(R.id.five);
        six=findViewById(R.id.six);
        seven=findViewById(R.id.seven);
        eight=findViewById(R.id.eight);
        nine=findViewById(R.id.nine);
        zero=findViewById(R.id.zero);
        plus=findViewById(R.id.plus);
        minus=findViewById(R.id.minus);
        div=findViewById(R.id.div);
        into=findViewById(R.id.into);
        clear=findViewById(R.id.cancel);
        equal=findViewById(R.id.equal);
        percent= findViewById(R.id.percent);
        screenTv=findViewById(R.id.screen);
        exp="";

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"1";
                screenTv.setText(exp);
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"2";
                screenTv.setText(exp);
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"3";
                screenTv.setText(exp);
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"4";
                screenTv.setText(exp);
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"5";
                screenTv.setText(exp);
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"6";
                screenTv.setText(exp);
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"7";
                screenTv.setText(exp);
            }
        });


        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"8";
                screenTv.setText(exp);
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"9";
                screenTv.setText(exp);
            }
        });


        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exp=exp+"0";
                screenTv.setText(exp);
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                operation=1;
                number1=Double.parseDouble(exp);
                exp="";
                screenTv.setText(exp);
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operation=2;
                number1=Double.parseDouble(exp);
                exp="";
                screenTv.setText(exp);
            }
        });
        into.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                operation=3;
                number1=Double.parseDouble(exp);
                exp="";
                screenTv.setText(exp);
            }
        });
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                operation=4;
                number1=Double.parseDouble(exp);
                exp="";
                screenTv.setText(exp);
            }
        });
        percent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number1=Double.parseDouble(exp);
                result= number1/100;
                exp="";
                screenTv.setText(String.valueOf(result));
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (exp.length()>0)
                exp=exp.substring(0,exp.length()-1);
              screenTv.setText(exp);

            }
        });
        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                number2=Double.parseDouble(exp);

                switch (operation){

                    case 1:
                        result =number1+number2;
                        break;
                    case 2:
                        result =number1-number2;
                        break;
                    case 3:
                        result =number1*number2;
                        break;
                    case 4:
                        result =number1/number2;
                        break;

                    default:
                        result =0;

                }

                exp="";
                screenTv.setText(String.valueOf((int) result));
            }
        });
    }
}
