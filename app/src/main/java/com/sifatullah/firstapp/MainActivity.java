package com.sifatullah.firstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

//    TextView tv1,tv2;
//    Button b1;
//    boolean bool;

    EditText et1,et2;
    Button plus,minus,multiply,divide,equal;
    TextView result;
    int num1,num2,operation;
    double sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);


//        bool=true;
//
//
//        tv1=(TextView) findViewById(R.id.text1);
//        tv2= (TextView) findViewById(R.id.text2);
//        b1= (Button) findViewById(R.id.button1);
//
//        b1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (bool){
//                    tv1.setText("CSE");
//                    tv2.setText("DU");
//                    bool=false;
//
//                }
//
//                else {
//
//                    tv1.setText("CSTE");
//                    tv2.setText("NSTU");
//                    bool=true;
//                }
//
//
//            }
//        });





        et1=(EditText)  findViewById(R.id.num1);
        et2= (EditText) findViewById(R.id.num2);
        plus=(Button) findViewById(R.id.plus);
        minus=(Button) findViewById(R.id.minus);
        multiply=(Button) findViewById(R.id.multiply);
        divide=(Button) findViewById(R.id.division);
        equal=(Button) findViewById(R.id.equal);
        result=(TextView) findViewById(R.id.res);


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operation=1;
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operation=2;
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operation=3;
            }
        });


        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operation=4;

            }
        });





        equal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                num1= Integer.parseInt(et1.getText().toString());
                num2= Integer.parseInt(et2.getText().toString());

                switch (operation){

                    case 1:
                        sum =num1+num2;
                        break;
                    case 2:
                        sum =num1-num2;
                        break;
                    case 3:
                        sum =num1*num2;
                        break;
                    case 4:
                        sum =num1/num2;
                        break;

                    default:
                        sum =0;

                }


                result.setText(String.valueOf(sum));
            }
        });






    }
}
